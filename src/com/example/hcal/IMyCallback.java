package com.example.hcal;

import android.view.View;

/**
 * Created with IntelliJ IDEA.
 * User: HCorbin
 * Date: 3/16/13
 * Time: 10:28 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IMyCallback {
    public void onComplete();
    public void onCellClicked(View view);
}
