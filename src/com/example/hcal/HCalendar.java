package com.example.hcal;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: HCorbin
 * Date: 3/15/13
 * Time: 1:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class HCalendar {
    TableLayout cal;
    public Map<Integer,String> gridIndexToDay=new HashMap<Integer,String>();
    public Calendar currentDate;
    Context _context;
    String[] _logs;
    IMyCallback _callback;

    public List<Integer> IgnoreIndexes=new ArrayList<Integer>();
    public HCalendar(View layout, IMyCallback callback,String[] logs){
        cal = (TableLayout)layout;
        _context = layout.getContext();
        _callback = callback;
        _logs= logs;

        Initialize();
    }

    public void Initialize(){
        if(currentDate == null)
            currentDate = Calendar.getInstance();

        initializeGrid();
    }

    public void nextMonth(){
        currentDate.add(Calendar.MONTH, 1);
        Initialize();
    }

    public void previousMonth(){
        currentDate.add(Calendar.MONTH, -1);
        Initialize();
    }

    private void initializeGrid(){
        cal.removeAllViews();
        gridIndexToDay=new HashMap<Integer,String>();
        for(int i=0;i<49;i++){
            switch(i){
                case 0:
                    gridIndexToDay.put(i,"S");
                    break;
                case 1:
                    gridIndexToDay.put(i,"M");
                    break;
                case 2:
                    gridIndexToDay.put(i,"T");
                    break;
                case 3:
                    gridIndexToDay.put(i,"W");
                    break;
                case 4:
                    gridIndexToDay.put(i,"T");
                    break;
                case 5:
                    gridIndexToDay.put(i,"F");
                    break;
                case 6:
                    gridIndexToDay.put(i,"S");
                    break;
                default:
                    gridIndexToDay.put(i,"-1");
            }
        }
        getGridIndexMap();
        populateTable();
    }

    private void populateTable() {
        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        List<TableRow> rows = new ArrayList<TableRow>();
        for(int i=0;i<7;i++)
        {
            TableRow newRow = new TableRow(_context);
            newRow.setLayoutParams(new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.FILL_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
            rows.add(newRow);
        }

        //iterate over the hashmap and add views to the tablelayout
        for(int i=0;i<49;i++){
            TableRow currentRow=null;
            if(i >= 0 && i <= 6){
                View dayOfWeekCell  =   inflater.inflate(R.layout.dayofweek, null);

                currentRow = rows.get(0);
                currentRow.setBackgroundResource(R.drawable.redbutton);
                TextView _text = (TextView) dayOfWeekCell
                        .findViewById(R.id.grid_item_label);
                ImageView _image = (ImageView)dayOfWeekCell.findViewById(R.id.grid_image);
                _text.setText(gridIndexToDay.get(i));
                currentRow.addView(dayOfWeekCell);
            }
            else{
                for(TableRow r : rows){
                    if(r.getChildCount() < 7)  {
                        currentRow=r;
                        break;
                    }
                }

                if(isEnabled(i)){
                    View dayEnabledCell=null;
                    if(Integer.valueOf(gridIndexToDay.get(i)) == Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                    {
                        dayEnabledCell=inflater.inflate(R.layout.dayactive, null);
                    }
                    else{
                        dayEnabledCell=inflater.inflate(R.layout.day, null);
                    }


                    TextView _text = (TextView) dayEnabledCell
                            .findViewById(R.id.grid_item_label);
                    ImageView _image = (ImageView)dayEnabledCell.findViewById(R.id.grid_image);
                    _text.setText(gridIndexToDay.get(i));

                    //log for this date, show image
                    //this is just custom logic to decide when to show the image
                    //you may want to always show the image and can adjust logic to do so
                    if(_logs != null && _logs.length > 0 && isEnabled(i)){
                        _image.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        _image.setImageResource(R.drawable.checkmark);
                    }

                    //setup click event
                    dayEnabledCell.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            _callback.onCellClicked(v);
                        }
                    });

                    currentRow.addView(dayEnabledCell);
                }
                else{
                    //set to disabled cell
                    View dayDisabledCell = inflater.inflate(R.layout.daydisabled, null);

                    TextView _text = (TextView) dayDisabledCell
                            .findViewById(R.id.grid_item_label);
                    ImageView _image = (ImageView)dayDisabledCell.findViewById(R.id.grid_image);
                    _text.setText(gridIndexToDay.get(i));
                    currentRow.addView(dayDisabledCell);
                }
            }
        }

        //setup button row
        RelativeLayout buttons  =  (RelativeLayout)inflater.inflate(R.layout.calendarbuttons, null);
        final TextView monthView = (TextView)buttons.findViewById(R.id.monthView);
        monthView.setText(currentDate.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US)+" " + String.valueOf(Integer.valueOf(currentDate.get(Calendar.YEAR))));

        Button nextMonth = (Button)buttons.findViewById(R.id.nextMonth);
        Button previousMonth = (Button)buttons.findViewById(R.id.previousMonth);
        nextMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextMonth();
                monthView.setText(currentDate.getDisplayName(Calendar.MONTH,Calendar.LONG, Locale.US)+" " + String.valueOf(Integer.valueOf(currentDate.get(Calendar.YEAR))));
            }
        });
        previousMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousMonth();
                monthView.setText(currentDate.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US)+" " + String.valueOf(Integer.valueOf(currentDate.get(Calendar.YEAR))));
            }
        });

        TableRow buttonRow = new TableRow(_context);
        TableRow.LayoutParams params = new TableRow.LayoutParams(
                TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        params.span = 7;
        buttonRow.addView(buttons,params);
        cal.addView(buttonRow);

        //add rest of rows
        for(TableRow row : rows){
            cal.addView(row);
        }
    }

    public boolean isEnabled(int position){
        if(position >= 0 && position <= 6){
            return false;
        }

        if(IgnoreIndexes.contains(position)){
            return false;
        }
        return true;
    }

    public Map<Integer,String> getGridIndexMap(){
        IgnoreIndexes =new ArrayList<Integer>();
        int start=getMonthStartDay()+7; //add 7 to account for days of week row
        int dayCounter=1;
        int dayCount = getMonthDayCount();
        int lastDayIndex=0;
        for(int i=start;i<dayCount+start;i++){
            gridIndexToDay.put(i,String.valueOf(dayCounter));
            lastDayIndex = i;
            dayCounter++;
        }

        int maxPreviousDays = getPreviousMonthDays();
        for(int i = start-1;i>=7;i--){
            gridIndexToDay.put(i,String.valueOf(maxPreviousDays));
            IgnoreIndexes.add(i);
            maxPreviousDays--;
        }

        int maxNextDays = 1;
        if(lastDayIndex < 49){
            for(int i = lastDayIndex + 1;i<=48;i++){
                gridIndexToDay.put(i,String.valueOf(maxNextDays));
                IgnoreIndexes.add(i);
                maxNextDays++;
            }
        }

        return gridIndexToDay;
    }

    private int getPreviousMonthDays(){
        int currentMonthNum = currentDate.get(Calendar.MONTH);
        int previousMonthNum=0;
        Calendar previousMonthCal = Calendar.getInstance();

        if(currentMonthNum > 0) {
            previousMonthNum = currentMonthNum - 1;
            previousMonthCal.set(currentDate.get(Calendar.YEAR),previousMonthNum,1);
            return previousMonthCal.getActualMaximum(Calendar.DAY_OF_MONTH);
        }
        else {
            previousMonthNum = 11;
            previousMonthCal.set(currentDate.get(Calendar.YEAR)-1,previousMonthNum,1);
            return previousMonthCal.getActualMaximum(Calendar.DAY_OF_MONTH);
        }
    }

    private int getMonthDayCount(){
        return currentDate.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    private int getMonthStartDay(){
        Calendar firstDay = currentDate;
        firstDay.set(Calendar.DAY_OF_MONTH, 1);
        return firstDay.get(Calendar.DAY_OF_WEEK)-1;
    }
}
