package com.example.hcal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    private String[] log_array = new String[]{"test1", "test2", "test3"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Setup_Calendar();
    }

    private void Setup_Calendar() {
        TableLayout hcalendar = (TableLayout)findViewById(R.id.hcalendar);
        HCalendar hcal = new HCalendar(hcalendar, new MyCallback(){
            @Override
            public void onCellClicked(View view) {
                //Here you can wire up your custom actions for calendar day clicks
                Toast.makeText(view.getContext(), "Cell clicked", Toast.LENGTH_SHORT).show();
            }
        },log_array);
    }
}
